package module

import (
	"gitee.com/wanjimao/platform-gitee/buddy/internal/app/service/private"
	"gitee.com/wanjimao/platform-gitee/buddy/internal/app/service/public"
	fx2 "gitee.com/wanjimao/platform-gitee/buddy/pkg/fx"
	"gitee.com/wanjimao/platform-gitee/buddy/pkg/metadata"
	"go.uber.org/fx"
)

var (
	PublicModule = fx.Module(metadata.AppId,
		public.ServiceModule,
		fx2.BuddyClientModule,
		fx2.BuddySettingsModule,
		fx2.StoreProviderModule,
		fx2.BuddyMemoryModule,
	)
	PrivateModule = private.ServiceModule
)
