package main

import (
	"gitee.com/wanjimao/platform-gitee/buddy/internal/app/service/external"
	"gitee.com/wanjimao/platform-gitee/buddy/internal/app/service/private"
	"gitee.com/wanjimao/platform-gitee/buddy/internal/app/service/public"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
)

func main() {
	fxsvcapp.StandardMain(
		external.ServiceModules,
		public.ServiceModule,
		private.ServiceModule,
	)
}
