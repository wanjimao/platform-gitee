package external

import (
	"go.uber.org/fx"

	ffx "gitee.com/wanjimao/platform-gitee/buddy/pkg/fx"
)

var ServiceModules = fx.Options(
	ffx.BuddySettingsModule,
)
