package private

import (
	"context"
	"gitee.com/wanjimao/platform-gitee/common/nosql/document"
	pb "gitee.com/wanjimao/platform-gitee/proto/gen/buddy/api"

	"gitee.com/wanjimao/platform-gitee/auth/pkg/authdb"
	"gitee.com/wanjimao/platform-gitee/buddy/internal/db"
	bfx "gitee.com/wanjimao/platform-gitee/buddy/pkg/fx"
	"gitee.com/wanjimao/platform-gitee/buddy/pkg/metadata"
	"gitee.com/wanjimao/platform-gitee/common/access"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/common/service"

	"go.uber.org/fx"
	"go.uber.org/zap"
)

type Service struct {
	service.Logger
	service.TcpTransport
	appId      string
	db         db.Database
	deployment string
}

func NewService(
	l *zap.Logger,
	rosStore document.DocumentStore,
	deployment string,
) (result *Service, err error) {
	result = &Service{
		appId:      metadata.AppId,
		db:         db.OpenDatabase(l, rosStore),
		deployment: deployment,
	}
	return
}

func (s *Service) AccessLevel() access.AccessLevel {
	return access.AccessUndefined
}

func (s *Service) ValidateSettings() error {

	return s.db.ValidateSettings()
}

func (s *Service) RegisterWithGrpcServer(server service.HasGrpcServer) error {
	pb.RegisterPrivateServiceServer(server.GrpcServer(), s)
	return nil
}

func (s *Service) sessionFromContext(ctx context.Context) (*authdb.Session, error) {
	if session, ok := authdb.SessionFromContext(ctx); !ok {
		return nil, ErrNoMetaData
	} else {
		return session, nil
	}
}

var ServiceModule = fx.Provide(
	func(
		l *zap.Logger,
		ac fxsvcapp.GlobalAuthClient,
		rdb fxsvcapp.GlobalRosDataStore,
		s fxsvcapp.GlobalSettings,
		fs bfx.BuddySettings,
	) (out service.GrpcServiceFactory, err error) {
		if svc, e := NewService(
			l,
			rdb.RosDataStore,
			s.Deployment,
		); e != nil {
			err = e
		} else if e = svc.ValidateSettings(); e != nil {
			err = e
		} else {
			out.GrpcService = svc
		}
		return
	},
)
