package module

import (
	"gitee.com/wanjimao/platform-gitee/storage/internal/app/service/public"
)

var (
	PublicModule = public.ServiceModule
)
