package main

import (
	"math/rand"
	"os"
	"path"
	"time"

	auth "gitee.com/wanjimao/platform-gitee/auth/pkg/module"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/guild/internal/app/service/external"
	"gitee.com/wanjimao/platform-gitee/guild/internal/app/service/public"
	ffx "gitee.com/wanjimao/platform-gitee/guild/pkg/fx"
)

func main() {
	rand.Seed(time.Now().Unix())
	home := os.Getenv("HOME")
	setEnvVariable("DOCUMENT_STORE_URL", "badger://badger/"+path.Join(home, "notification"))

	fxsvcapp.StandardMain(
		ffx.GuildSettingsModule,
		external.ServicesModules,
		public.ServiceModule,
		fxsvcapp.AuthStoreModule,
		auth.PublicModule,
	)
}

func setEnvVariable(key string, value string) {
	if os.Getenv(key) == "" {
		_ = os.Setenv(key, value)
	}
}
