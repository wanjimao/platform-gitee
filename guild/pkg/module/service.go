package module

import (
	"gitee.com/wanjimao/platform-gitee/guild/internal/app/service/public"
)

var (
	PublicModule = public.ServiceModule
)
