package module

import (
	"gitee.com/wanjimao/platform-gitee/mail/internal/app/service/public"
	fx3 "gitee.com/wanjimao/platform-gitee/mail/pkg/fx"
	"go.uber.org/fx"
)

var (
	PublicModule = fx.Options(
		public.ServiceModule,
		fx3.MailClientModule,
		fx3.MailSettingsModule,
	)
)
