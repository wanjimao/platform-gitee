package main

import (
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/common/mq/mock"
	"gitee.com/wanjimao/platform-gitee/mail/internal/app/service/external"
	"gitee.com/wanjimao/platform-gitee/mail/internal/app/service/public"
	ffx "gitee.com/wanjimao/platform-gitee/mail/pkg/fx"
	"os"
	"path"
)

func main() {
	home := os.Getenv("HOME")
	setEnvVariable("DOCUMENT_STORE_URL", "badger://badger/"+path.Join(home, "mail"))
	setEnvVariable("MEMORY_STORE_URL", "redis://127.0.0.1:6379")

	fxsvcapp.StandardMain(
		ffx.MailSettingsModule,
		public.ServiceModule,
		external.ServicesModules,
		fxsvcapp.MessageQueueModule,
		mock.MQModule,
	)
}

func setEnvVariable(key string, value string) {
	if os.Getenv(key) == "" {
		_ = os.Setenv(key, value)
	}
}
