package module

import (
	"gitee.com/wanjimao/platform-gitee/chat/internal/app/service/public"
	fx2 "gitee.com/wanjimao/platform-gitee/chat/pkg/fx"
	"go.uber.org/fx"
)

var Module = fx.Module("chat", fx.Options(
	public.ServiceModule,
	fx2.ChatSettingsModule,
	fx2.StoreProviderModule,
	fx2.ChatMemoryModule,
))
