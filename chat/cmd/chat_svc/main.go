package main

import (
	"gitee.com/wanjimao/platform-gitee/chat/internal/app/service/external"
	"gitee.com/wanjimao/platform-gitee/chat/internal/app/service/public"
	"gitee.com/wanjimao/platform-gitee/chat/pkg/fx"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
)

func main() {
	fxsvcapp.StandardMain(
		external.ServiceModules,
		public.ServiceModule,
		fx.StoreProviderModule,
	)
}
