// Alias types declared in the code generated service to simplify referencing.
package public

import pb "gitee.com/wanjimao/platform-gitee/proto/gen/chat/api"

type ChatMessage = pb.ChatMessage
type ChatServer = pb.ChatService_ChatServer
