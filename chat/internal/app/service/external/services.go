package external

import (
	cfx "gitee.com/wanjimao/platform-gitee/chat/pkg/fx"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/common/mq/nats"
	"go.uber.org/fx"
)

var ServiceModules = fx.Options(
	fxsvcapp.MessageQueueModule,
	nats.MQModule,
	cfx.ChatSettingsModule,
)
