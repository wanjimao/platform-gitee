package db

import (
	"gitee.com/wanjimao/platform-gitee/common/nosql/memory/keys"
)

func makeChatMsgKey(uid string) (keys.Key, error) {
	return keys.NewKeyFromParts("chat", "message", "queue", uid)
}
