package main

import (
	"math/rand"
	"os"
	"path"
	"time"

	afx "gitee.com/wanjimao/platform-gitee/auth/pkg/fx"
	auth "gitee.com/wanjimao/platform-gitee/auth/pkg/module"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/notification/internal/app/service/public"
	ffx "gitee.com/wanjimao/platform-gitee/notification/pkg/fx"
)

func main() {
	rand.Seed(time.Now().Unix())
	home := os.Getenv("HOME")
	setEnvVariable("DOCUMENT_STORE_URL", "badger://badger/"+path.Join(home, "notification"))

	fxsvcapp.StandardMain(
		ffx.NotificationSettingsModule,
		public.ServiceModule,
		auth.PublicModule,
		afx.AuthSettingsModule,
		fxsvcapp.AuthStoreModule,
		fxsvcapp.AuthClientModule,
	)
}

func setEnvVariable(key string, value string) {
	if os.Getenv(key) == "" {
		_ = os.Setenv(key, value)
	}
}
