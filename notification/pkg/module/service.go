package module

import (
	"gitee.com/wanjimao/platform-gitee/notification/internal/app/service/public"
)

var (
	PublicModule = public.ServiceModule
)
