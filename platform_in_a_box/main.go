package main

import (
	auth "gitee.com/wanjimao/platform-gitee/auth/pkg/module"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/common/mq/mock"
	"gitee.com/wanjimao/platform-gitee/condition/pkg/fx"
	condition "gitee.com/wanjimao/platform-gitee/condition/pkg/module"
	"os"
	"path"
)

func main() {
	home := os.Getenv("HOME")
	setEnvVariable("DOCUMENT_STORE_URL", "badger://badger/"+path.Join(home, "room"))
	setEnvVariable("MEMORY_STORE_URL", "redis://127.0.0.1:6379")
	fxsvcapp.StandardMain(
		fxsvcapp.MessageQueueModule,
		mock.MQModule,
		auth.PublicModule,
		fxsvcapp.AuthStoreModule,
		fxsvcapp.ConditionStoreModule,
		condition.PublicModule,
		fx.ConditionSettingsModule,
	)
}

func setEnvVariable(key string, value string) {
	if os.Getenv(key) == "" {
		_ = os.Setenv(key, value)
	}
}
