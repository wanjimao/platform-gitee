package fx

import (
	"go.uber.org/fx"
	"go.uber.org/zap"

	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	gm "gitee.com/wanjimao/platform-gitee/gm/pkg/client"
)

type GmClientParams struct {
	fx.In
	GmClient gm.GmClient `name:"GmClient"`
}

type GmClientResult struct {
	fx.Out
	GmClient gm.GmClient `name:"GmClient"`
}

func (g *GmClientResult) Execute(
	l *zap.Logger,
	t fxsvcapp.GlobalTracer,
	s fxsvcapp.SecuritySettings,
	a GMSettings,
) (err error) {
	l.Info("connect",
		zap.String("service", "gmclient"),
		zap.String("url", a.GmUrl),
	)
	g.GmClient, err = gm.NewGMClient(a.GmUrl, s.SecureClients)

	return
}

var GmClientModule = fx.Provide(
	func(
		l *zap.Logger,
		t fxsvcapp.GlobalTracer,
		s fxsvcapp.SecuritySettings,
		a GMSettings,
	) (out GmClientResult, err error) {
		err = out.Execute(l, t, s, a)
		return
	},
)
