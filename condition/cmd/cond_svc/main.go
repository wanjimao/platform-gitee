package main

import (
	auth "gitee.com/wanjimao/platform-gitee/auth/pkg/module"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/common/mq/mock"
	"gitee.com/wanjimao/platform-gitee/condition/internal/app/service/external"
	"gitee.com/wanjimao/platform-gitee/condition/internal/app/service/public"
	"gitee.com/wanjimao/platform-gitee/condition/pkg/fx"
	"os"
	"path"
)

func main() {
	home := os.Getenv("HOME")
	setEnvVariable("DOCUMENT_STORE_URL", "badger://badger/"+path.Join(home, "room"))
	setEnvVariable("MEMORY_STORE_URL", "redis://127.0.0.1:6379/0")

	fxsvcapp.StandardMain(
		fx.ConditionSettingsModule,
		public.ServiceModule,
		external.ServicesModules,
		auth.PublicModule,
		fxsvcapp.AuthStoreModule,
		fxsvcapp.MessageQueueModule,
		mock.MQModule,
	)
}

func setEnvVariable(key string, value string) {
	if os.Getenv(key) == "" {
		_ = os.Setenv(key, value)
	}
}
