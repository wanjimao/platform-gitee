package public

import (
	"gitee.com/wanjimao/platform-gitee/common/mq"
	pb "gitee.com/wanjimao/platform-gitee/condition/gen/condition/api"
	"gitee.com/wanjimao/platform-gitee/condition/internal/app/db"
	fx2 "gitee.com/wanjimao/platform-gitee/condition/pkg/fx"
	"gitee.com/wanjimao/platform-gitee/condition/pkg/metadata"
	"go.uber.org/fx"
	"go.uber.org/zap"

	"gitee.com/wanjimao/platform-gitee/common/access"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/common/nosql/memory"
	"gitee.com/wanjimao/platform-gitee/common/service"
)

type Service struct {
	service.TcpTransport
	appId      string
	logger     *zap.Logger
	deployment string
	db         *db.Database
	url        string
	mq         mq.MessageQueue
}

func (s *Service) RegisterWithGrpcServer(server service.HasGrpcServer) error {
	pb.RegisterConditionServiceServer(server.GrpcServer(), s)
	return nil
}

func (s *Service) RegisterWithGatewayServer(server service.HasGatewayServer) error {
	return nil
}

func NewService(
	l *zap.Logger,
	deployment string,
	memory memory.MemoryStore,
	url string,
	mq mq.MessageQueue,
) (result *Service, err error) {
	result = &Service{
		appId:      metadata.AppId,
		logger:     l,
		deployment: deployment,
		db:         db.OpenDatabase(l, memory),
		url:        url,
		mq:         mq,
	}
	return
}

func (s *Service) AccessLevel() access.AccessLevel {
	return access.AccessUndefined
}

var ServiceModule = fx.Provide(
	func(
		l *zap.Logger,
		s fxsvcapp.GlobalSettings,
		fs fx2.ConditionSettings,
		memory fxsvcapp.GlobalConditionServerStore,
		mq fxsvcapp.GlobalMQ,
	) (out service.GrpcServiceFactory, err error) {
		if svc, e := NewService(
			l,
			s.Deployment,
			memory.ConditionServerStore,
			fs.ConditionUrl,
			mq.MessageQueue,
		); e != nil {
			err = e
		} else {
			err = out.Execute(svc)
		}
		return
	},
)
