package module

import (
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
	"gitee.com/wanjimao/platform-gitee/condition/internal/app/service/public"
	fx2 "gitee.com/wanjimao/platform-gitee/condition/pkg/fx"
	"gitee.com/wanjimao/platform-gitee/condition/pkg/metadata"
	"go.uber.org/fx"
)

var (
	PublicModule = fx.Module(
		metadata.AppId,
		public.ServiceModule,
		fx2.ConditionClientModule,
		fx2.ConditionSettingsModule,
		fxsvcapp.ConditionStoreModule,
	)
)
