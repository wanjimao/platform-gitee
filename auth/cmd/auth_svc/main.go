package main

import (
	"gitee.com/wanjimao/platform-gitee/auth/internal/app/service/external"
	"gitee.com/wanjimao/platform-gitee/auth/internal/app/service/public"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
)

func main() {
	fxsvcapp.StandardMain(
		public.ServiceModule,
		external.ServicesModules,
	)
}
