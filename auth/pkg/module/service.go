package module

import (
	"gitee.com/wanjimao/platform-gitee/auth/internal/app/service/public"
	"gitee.com/wanjimao/platform-gitee/auth/pkg/metadata"
	"go.uber.org/fx"
)

var (
	Modules = fx.Module(metadata.AppId,
		public.ServiceModule,
	)
)
