package db

import (
	"go.uber.org/zap"

	"gitee.com/wanjimao/platform-gitee/common/nosql/document"
)

type Document struct {
	document.DocumentStore
	logger *zap.Logger
}

func OpenDatabase(l *zap.Logger, doc document.DocumentStore) *Document {
	return &Document{
		doc,
		l,
	}
}
