package external

import (
	"go.uber.org/fx"

	"gitee.com/wanjimao/platform-gitee/common/fx/fxsvcapp"
)

var ServicesModules = fx.Options(
	fxsvcapp.AuthStoreModule,
)
