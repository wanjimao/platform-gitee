package fxsvcapp

import (
	"go.uber.org/fx"

	afx "gitee.com/wanjimao/platform-gitee/auth/pkg/fx"
	"gitee.com/wanjimao/platform-gitee/common/fx/fxapp"
	"gitee.com/wanjimao/platform-gitee/common/logging"
	nfx "gitee.com/wanjimao/platform-gitee/common/nosql/pkg/fx"
	tfx "gitee.com/wanjimao/platform-gitee/common/tracing/pkg/fx"
)

var AppNameModule = fx.Provide(
	func() (out fxapp.AppNameLoader, err error) {
		err = out.LoadFromExecutable()
		return
	},
)

var LoggingModule = fx.Options(
	fxapp.NewAppLifecycleLogger(),
	logging.Module,
)

var HttpDebugModule = fx.Provide(
	NewTcpDebugHttpService,
)

var HttpMetricsModule = fx.Provide(
	NewMetricsHttpService,
)

var ServicesModule = fx.Options(
	AppNameModule,
	SettingsModule,
	afx.SettingsModule,
	tfx.TracerSettingsModule,
	LoggingModule,
	ConnectionMuxModule,
	ServersModule,
	TracerModule,
	SecuritySettingsModule,
	AuthClientModule,
	nfx.NoSQLSettingsModule,
	DocumentStoreProviderModule, //引入mangodb
	MemoryStoreProviderModule,
	RedisStoreProviderModule,
)

func StandardMain(opts ...fx.Option) {
	if err := fxapp.Main(
		ServicesModule,
		fx.Options(opts...),
		fx.Invoke(cli),
	); err != nil {
		panic(err)
	}
}
