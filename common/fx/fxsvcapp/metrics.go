package fxsvcapp

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitee.com/wanjimao/platform-gitee/common/access"
	"gitee.com/wanjimao/platform-gitee/common/service"
)

type MetricsHttpService struct {
	service.TcpTransport
}

func (s *MetricsHttpService) RegisterWithHttpServer(server service.HasHttpServeMux) {
	h := server.HttpServeMux()
	h.Handle("/metrics", promhttp.Handler())
}

func NewMetricsHttpService() (out service.HttpServiceFactory) {
	out.HttpService = &MetricsHttpService{}
	return
}

func (s *MetricsHttpService) AccessLevel() access.AccessLevel {
	return access.AccessUndefined
}
