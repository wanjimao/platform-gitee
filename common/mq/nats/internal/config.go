package internal

import "gitee.com/wanjimao/platform-gitee/common/mq"

const (
	defaultDeliverySemantics = mq.AtLeastOnce
)
