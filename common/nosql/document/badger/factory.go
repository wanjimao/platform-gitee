package badger

import (
	"gitee.com/wanjimao/platform-gitee/common/nosql/document"
	"time"

	"github.com/dgraph-io/badger"

	"gitee.com/wanjimao/platform-gitee/common/nosql/document/badger/internal"
	"go.uber.org/zap"
)

func NewDocumentStoreProvider(dir string, gcInterval time.Duration, l *zap.Logger) (document.DocumentStoreProvider, error) {
	return internal.NewDocumentStoreProvider(dir, gcInterval, l), nil
}

func NewBadgerStore(dir string, name string) (*badger.DB, error) {
	return internal.NewBadgerStore(dir, name)
}
