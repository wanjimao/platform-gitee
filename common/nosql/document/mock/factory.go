package mock

import (
	"gitee.com/wanjimao/platform-gitee/common/nosql/document"
	"gitee.com/wanjimao/platform-gitee/common/nosql/document/mock/internal"
)

type DocumentStoreProvider = internal.DocumentStoreProvider

func NewDocumentStoreProvider() (document.DocumentStoreProvider, error) {
	return internal.NewDocumentStoreProvider()
}
