package couchbase

import (
	"gitee.com/wanjimao/platform-gitee/common/nosql/document"
	"gitee.com/wanjimao/platform-gitee/common/nosql/document/couchbase/internal"
	"go.uber.org/zap"
)

func NewDocumentStoreProvider(config ClusterConfig, l *zap.Logger) (document.DocumentStoreProvider, error) {
	return internal.NewDocumentStoreProvider(config.ConnUrl, config.Username, config.Password, l)
}
