package redis

import (
	"go.uber.org/zap"

	"gitee.com/wanjimao/platform-gitee/common/nosql/memory"
	"gitee.com/wanjimao/platform-gitee/common/nosql/memory/redis/internal"
)

type MemoryStoreProvider = internal.MemoryStoreProvider

func NewMemoryStoreProvider(addr string, password string, l *zap.Logger) (memory.MemoryStoreProvider, error) {
	return internal.NewMemoryStoreProvider(addr, password, l)
}
